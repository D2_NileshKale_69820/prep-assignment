#include <stdio.h>
#include <string.h>

struct Student
{
    char studentName[30];
    int marks;
    char rollNo[10];
};


int main()
{
    struct Student s1;
    printf("Enter name of student:");
    scanf("%s", &s1.studentName);
    printf("Enter roll number of %s:", s1.studentName);
    scanf("%s", &s1.rollNo);
    printf("Enter marks of %s:", s1.studentName);
    scanf("%s", &s1.marks);

    printf("Name: %s\n",s1.studentName);
    printf("Roll no.: %s\n",s1.rollNo);
    printf("Marks: %d\n",s1.marks);
}
